/**
 * Module dependencies.
 */
const express = require('express');
const cors = require('cors');
const moment = require('moment');

/**
 * Env
 */
if(!process.env.NODE_ENV) require('dotenv').config();

/**
 * Configuration
 */
global.__dirname = __dirname;
const version = process.env.VERSION;
const DEV_MODE = !process.env.NODE_ENV || process.env.NODE_ENV === "development";
/**
 * Middlewares
 * Uncomment to use it
 */
//const controls = require('./' + version + "/middlewares/controls.mw");
/**
 * DB Require
 * Uncomment to use database
 */
const db = require('./core/db/db');
/**
 * Helpers & Utils
 */
const Returns = require(`./${version}/utils/returns.utils`);

/****************************/

/**
 * DB Connection
 * Uncomment to use connector
 */
db.mongooseConnection();
//db.mongoConnection();

/**
 * Modules Configuration
 */
moment.locale('fr');

/**
 * Routers
 */
const API_ROUTER_ADMIN = require(`./${version}/routers/admin`).router;
const API_ROUTER_CLIENT = require(`./${version}/routers/client`).router;

/**
 * Global
 */
global.ERROR = Returns.error;
global.SUCCESS = Returns.success;
global.VALIDATOR = require(`./${version}/utils/validators.utils`);
global.ASYNC = require('async');

/**
 * Declare express
 */
const app = express();

/**
 * Express setup
 */
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

/**
 * Logger
 */
if(DEV_MODE){
  app.use(require('morgan')('dev'));
}

/**
 * Auth Middleware
 * Comment to use with or whitout auth middleware
 * Add when using api with nuxtjs
 */
//app.use('/'+version+'/', controls.auth, router);

/**
 * Routers
 */
app.use(`/${version}/client`, API_ROUTER_CLIENT);
app.use(`/${version}/admin`, API_ROUTER_ADMIN);

/**
 * Catch 404
 */
app.use(function (req, res) {
  res.status(404).json({success: false, message: "Route doesn't exist"});
});

/**
 * Error handler
 */
app.use(function (err, req, res, next) {
  if(DEV_MODE){
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500).json({success: false, message: err.message});
  } else {
    res.status(500).json({success: false, message: 'Internal Error'});
  }
});

module.exports = app;
