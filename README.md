# Node.js API Starter

OUGA OUGA add "trim: true" to field on model [exemple](https://stackoverflow.com/questions/20766360/whats-the-meaning-of-trim-when-use-in-mongoose)

### Node.js

Before all, you need [Node.js](https://nodejs.org/en/) and NPM installed and up to date.

### Database

The app include the ODM [Mongoose](https://mongoosejs.com/) for Mongo database management. 

The Mongoose Client Connection
```Javascript
db.mongooseConnection()
```

## .env

Add a .env file in the root of your project, ask to Thomas le gros bg if you don't have one.

## Installation

### Init

Before start coding, do not forget to install all depedencies

```bash
npm i
```

If you run the app for the first time, you need to run this command.

Development mode :

```bash
npm run dev-init
```
Production mode :
```bash
npm run prod-init
```

### Run the app
After the init command, to start the app you can run this command.

```bash
npm run dev
```
Production mode :
```bash
npm run start
```
The app start listening  at http://localhost:4000/v1/client or admin

## API Auth

This starter includes an auth middleware to protect your API. Uncomment the lines in `app.js` to use it. This middleware :
* Verify the headers
* Compare the time between each request sending
* Create and compare the secure signature sent by client

## Finished

And that's it ! Your can now code your features, your server is running correctly. 💪