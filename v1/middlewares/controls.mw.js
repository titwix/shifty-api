/**
 * Module dependencies.
 */
const moment = require('moment');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

/**
 * Models
 */
const Sessions = require('../models/sessions.model');
const SessionCustomers = require('../models/sessions.customer.model');
const Users = require('../models/users.model');

/**
 * Configuration
 */
const PRIVATE_KEY = process.env.API_KEY_PRIVATE;
const PUBLIC_KEY = process.env.API_KEY_PUBLIC;
const PRIVATE_KEY_CUSTOMER = process.env.API_KEY_PRIVATE_CUSTOMER;
const PUBLIC_KEY_CUSTOMER = process.env.API_KEY_PUBLIC_CUSTOMER;

module.exports = {
  auth: function (req, res, next) {

    let publicKey = req.headers['x-public-key'];
    let dateTime = req.headers['x-datetime'];
    let signature = req.headers['x-signature'];

    let method = req.method.toUpperCase();
    let uri = req.originalUrl;

    /**
     * Verify if all header elements are present
     */
    if (!publicKey || !dateTime || !signature) return res.status(400).json(global.ERROR("Missing header parameters", "AUTH"));
    /**
     * Checking public key
     */
    if (publicKey !== PUBLIC_KEY) return res.status(401).json(global.ERROR("Public key unknown", "AUTH"));

    /**
     * Check the time between request and asking
     * > 5mn is failed
     */
    let timeDiff = moment.utc().diff(Number(dateTime), 'minutes');
    if (timeDiff > 5) return res.status(409).json(global.ERROR("Request expired", "AUTH"));

    /**
     * Create the signature
     */
    let serverSign = method + uri + dateTime;
    let cryptoSign = crypto.createHmac('sha1', PRIVATE_KEY).update(serverSign, "utf-8").digest('hex');

    /**
     * Compare the signature
     */
    if (signature.toString() !== cryptoSign.toString()) return res.status(401).json(global.ERROR("Unauthorized", "AUTH"));

    /**
     * Go to next function is everything is ok
     */
    return next();
  },
  authCustomers: function (req, res, next) {

    let publicKey = req.headers['x-public-key'];
    let dateTime = req.headers['x-datetime'];
    let signature = req.headers['x-signature'];

    let method = req.method.toUpperCase();
    let uri = req.originalUrl;

    /**
     * Verify if all header elements are present
     */
    if (!publicKey || !dateTime || !signature) return res.status(400).json(global.ERROR("Missing header parameters", "AUTH"));
    /**
     * Checking public key
     */
    if (publicKey !== PUBLIC_KEY_CUSTOMER) return res.status(401).json(global.ERROR("Public key unknown", "AUTH"));

    /**
     * Check the time between request and asking
     * > 5mn is failed
     */
    let timeDiff = moment.utc().diff(Number(dateTime), 'minutes');
    if (timeDiff > 5) return res.status(409).json(global.ERROR("Request expired", "AUTH"));

    /**
     * Create the signature
     */
    let serverSign = method + uri + dateTime;
    let cryptoSign = crypto.createHmac('sha1', PRIVATE_KEY_CUSTOMER).update(serverSign, "utf-8").digest('hex');

    /**
     * Compare the signature
     */
    if (signature.toString() !== cryptoSign.toString()) return res.status(401).json(global.ERROR("Unauthorized", "AUTH"));

    /**
     * Go to next function is everything is ok
     */
    return next();
  },
  session: function (req, res, next) {
    
    const headerAuth = req.headers['authorization'];
    const token = (headerAuth != null) ? headerAuth.replace('Bearer ', '') : null;
    let userId = null;

    //Get user ID from token
    if(token != null) {
      try {
        var jwtToken = jwt.verify(token, process.env.JWT_SIGN);
        if(jwtToken != null) {
          userId = jwtToken.userId;
        }
      } catch(err) {
        console.log(err)
        return res.status(500).json(global.ERROR("Cannot read token"));
      }
    }

    if(!token){
      return res.status(400).json(global.ERROR("Token is missing", "CONTROLS"));
    }
    if(!userId || !global.VALIDATOR.match(userId)){
      return res.status(400).json(global.ERROR("User id is missing or not valid", "CONTROLS"));
    }

    global.ASYNC.waterfall([
      function (done) {
        Sessions.findOne({user: userId, JWT: token}, (err, session) => {
          if(err) return res.status(500).json(global.ERROR("An error occurred"));

          if(session) return done(session)

          return res.status(400).json(global.ERROR("Token is expired", "CONTROLS"));
        })
      }
    ], function (session) {
        Users.findOne({_id: session.user, deleted: false}, (err, user) => {
          if(err) return res.status(500).json(global.ERROR("An error occurred"));

          if(user){
            req.user = {};
            req.user.session = {};
            req.user = user;
            req.user.session = session;
            return next();
          }

          return res.status(404).json(global.ERROR("User not found", "CONTROLS"));
        });
    });

    /*
    Sessions.findOne({user: userId, JWT: token}, (err, session) => {
      if(err) return res.status(500).json(global.ERROR("An error occurred"));

      if(session) {
        Users.findOne({_id: session.user, deleted: false}, (err, user) => {
          if(err) return res.status(500).json(global.ERROR("An error occurred"));

          if(user){
            req.user = {};
            req.user.session = {};
            req.user = user;
            req.user.session = session;
            return next();
          }

          return res.status(404).json(global.ERROR("User not found", "CONTROLS"));
        });
      } else {
        return res.status(400).json(global.ERROR("Token is expired", "CONTROLS"));
      }
    })
    */
  
  },
  sessionCustomers: function (req, res, next) {

    const headerAuth = req.headers['authorization'];
    const token = (headerAuth != null) ? headerAuth.replace('Bearer ', '') : null;
    let userId = null;

    //Get user ID from token
    if(token != null) {
      try {
        var jwtToken = jwt.verify(token, process.env.JWT_SIGN);
        if(jwtToken != null) {
          userId = jwtToken.userId;
        }
      } catch(err) {
        console.log(err)
        return res.status(500).json(global.ERROR("Cannot read token"));
      }
    }

    if(!token){
      return res.status(400).json(global.ERROR("Token is missing", "CONTROLS"));
    }
    if(!userId || !global.VALIDATOR.match(userId)){
      return res.status(400).json(global.ERROR("User id is missing or not valid", "CONTROLS"));
    }

    global.ASYNC.waterfall([
      function (done) {
        SessionCustomers.findOne({user: userId, JWT: token}, (err, session) => {
          if(err) return res.status(500).json(global.ERROR("An error occurred"));

          if(session) return done(session)

          return res.status(400).json(global.ERROR("Token is expired", "CONTROLS"));
        })
      }
    ], function (session) {
        Users.findOne({_id: session.user, deleted: false}, (err, user) => {
          if(err) return res.status(500).json(global.ERROR("An error occurred"));

          if(user){
            req.user = {};
            req.user.session = {};
            req.user = user;
            req.user.session = session;
            return next();
          }

          return res.status(404).json(global.ERROR("User not found", "CONTROLS"));
        });
    });
  }
};
