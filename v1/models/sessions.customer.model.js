const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let sessionCustomerSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        index: true,
        default: null
    },
    JWT: {
        type: String,
        index: true
    },
    metadata: {
        device: {
            type: String,
            default: "",
            trim: true
        },
        ip: {
            type: String,
            default: null,
            trim: true
        }
    },
    expireAt: {
        type: Date,
        default: Date.now,
        index: true, /* adds Index */
        expires: 0 /* automatically expires row */
    },
}, {timestamps: true, versionKey: false});

module.exports = mongoose.model('Sessions.Customer', sessionCustomerSchema);