const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let tableSchema = new Schema({
    name: {
        type: String,
        default: "-"
    },
    number: {
        type: Number,
        default: 1
    },
    restaurant: {
        type: Schema.Types.ObjectId,
        ref: 'Restaurants',
        index: true,
        default: null
    },
    seats: {
        type: Number,
        default: 1
    },
    deleted: {
        type: Boolean,
        default: false,
        index: true
    }
}, {timestamps: true, versionKey: false});

module.exports = mongoose.model('Tables', tableSchema);