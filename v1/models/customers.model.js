const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let customerSchema = new Schema({
    email: {
        type: String,
        default: null,
        trim: true,
        required: true
    },
    password: {
        type: String,
        default: null,
        required: false
    },
    image: {
        type: String,
        trim: true,
        default: "/default.png"
    },
    identity: {
        firstname: {
            type: String,
            default: null,
            trim: true
        },
        lastname: {
            type: String,
            default: null,
            trim: true
        },
        phone: {
            type: String,
            default: null,
            trim: true
        }
    },
    metadata: {
        is_register: {
            type: Boolean,
            default: false,
            index: true
        },
        mail_confirmed: {
            type: Boolean,
            default: false
        },
        accepted_tos: {
            type: Boolean,
            default: false
        }
    },
    deleted: {
        type: Boolean,
        default: false,
        index: true
    }
}, {timestamps: true, versionKey: false});

module.exports = mongoose.model('Customers', customerSchema);