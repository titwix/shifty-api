const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let restaurantSchema = new Schema({
    slug: {
        type: String,
        index: true
    },
    name: {
        type: String,
        trim: true
    },
    description: {
        type: String,
        trim: true,
        default: null
    },
    pictures: {
        logo: {
            type: String,
            trim: true,
            default: null
        },
        banner: []
    },
    position: {
        gps: {
            lat: {
                type: Number,
                default: null
            },
            long:{
                type: Number,
                default: null
            }
        },
        address: {
            street: {
                type: String,
                trim: true,
                default: null
            },
            zipcode: {
                type: String,
                trim: true,
                default: null
            },
            city: {
                type: String,
                trim: true,
                default: null
            },
            country: {
                type: String,
                trim: true,
                default: 'France'
            }
        }
    },
    contacts: {
        phone: {
            type: String,
            trim: true,
            default: null
        },
        email: {
            type: String,
            trim: true,
            default: null
        }
    },
    connects: {
        stripe: {
            active: {
                type: Boolean,
                default: false
            },
            publishable_key: {
                type: String,
                trim: true,
                default: null
            },
            secret_key: {
                type: String,
                trim: true,
                default: null
            }
        }
    },
    settings: {
        state: {
            type: String,
            trim: true,
            default: 'hidden',
            index: true,
            match: /^(published|hidden)$/
        },
        time: {
            morning: {
                start: {
                    type: String,
                    default: '11:30'
                },
                end: {
                    type: String,
                    default: '14:00'
                },
                interval: {
                    type: Number,
                    default: 60
                }
            },
            evening: {
                start: {
                    type: String,
                    default: '19:00'
                },
                end: {
                    type: String,
                    default: '23:00'
                },
                interval: {
                    type: Number,
                    default: 60
                }
            }
        },
        closed: [
            {
                from: {
                    type: Date,
                    default: Date.now
                },
                to: {
                    type: Date,
                    default: Date.now
                }
            }
        ]
    },
    room: {
        type: String,
        default: null
    },
    admin: {
        type: Schema.Types.ObjectId,
        ref: 'Users',
        index: true,
    },
    subscription: {
        type: Schema.Types.ObjectId,
        ref: 'Subscriptions',
        index: true,
        default: null
    },
    schedule: {
        type: Schema.Types.ObjectId,
        ref: 'Schedules',
        index: true,
        default: null
    },
    deleted: {
        type: Boolean,
        default: false,
        index: true
    }
}, {timestamps: true, versionKey: false});

module.exports = mongoose.model('Restaurants', restaurantSchema);