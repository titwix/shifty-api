const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let waiterSchema = new Schema({
    email: {
        type: String,
        default: null,
        trim: true,
        required: true
    },
    password: {
        type: String,
        default: null,
        required: false
    },
    image: {
        type: String,
        trim: true,
        default: '/default.png'
    },
    identity: {
        firstname: {
            type: String,
            default: null,
            trim: true
        },
        lastname: {
            type: String,
            default: null,
            trim: true
        }
    },
    restaurant: {
        type: Schema.Types.ObjectId,
        ref: 'Restaurants',
        index: true,
        default: null
    },
    admin: {
        type: Schema.Types.ObjectId,
        ref: 'Users',
        index: true,
        default: null
    },
    deleted: {
        type: Boolean,
        default: false,
        index: true
    }
}, {timestamps: true, versionKey: false});

module.exports = mongoose.model('Waiters', waiterSchema);