const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let scheduleSchema = new Schema({
    monday: {
        active: {
            type: Boolean,
            default: true
        },
        periods: {
            morning: {
                start: {
                    type: String,
                    default: null
                },
                end: {
                    type: String,
                    default: null
                }
            },
            evening: {
                start: {
                    type: String,
                    default: null
                },
                end: {
                    type: String,
                    default: null
                }
            }
        }
    },
    tuesday: {
        active: {
            type: Boolean,
            default: true
        },
        periods: {
            morning: {
                start: {
                    type: String,
                    default: null
                },
                end: {
                    type: String,
                    default: null
                }
            },
            evening: {
                start: {
                    type: String,
                    default: null
                },
                end: {
                    type: String,
                    default: null
                }
            }
        }
    },
    wednesday: {
        active: {
            type: Boolean,
            default: true
        },
        periods: {
            morning: {
                start: {
                    type: String,
                    default: null
                },
                end: {
                    type: String,
                    default: null
                }
            },
            evening: {
                start: {
                    type: String,
                    default: null
                },
                end: {
                    type: String,
                    default: null
                }
            }
        }
    },
    thursday: {
        active: {
            type: Boolean,
            default: true
        },
        periods: {
            morning: {
                start: {
                    type: String,
                    default: null
                },
                end: {
                    type: String,
                    default: null
                }
            },
            evening: {
                start: {
                    type: String,
                    default: null
                },
                end: {
                    type: String,
                    default: null
                }
            }
        }
    },
    friday: {
        active: {
            type: Boolean,
            default: true
        },
        periods: {
            morning: {
                start: {
                    type: String,
                    default: null
                },
                end: {
                    type: String,
                    default: null
                }
            },
            evening: {
                start: {
                    type: String,
                    default: null
                },
                end: {
                    type: String,
                    default: null
                }
            }
        }
    },
    saturday: {
        active: {
            type: Boolean,
            default: false
        },
        periods: {
            morning: {
                start: {
                    type: String,
                    default: null
                },
                end: {
                    type: String,
                    default: null
                }
            },
            evening: {
                start: {
                    type: String,
                    default: null
                },
                end: {
                    type: String,
                    default: null
                }
            }
        }
    },
    sunday: {
        active: {
            type: Boolean,
            default: false
        },
        periods: {
            morning: {
                start: {
                    type: String,
                    default: null
                },
                end: {
                    type: String,
                    default: null
                }
            },
            evening: {
                start: {
                    type: String,
                    default: null
                },
                end: {
                    type: String,
                    default: null
                }
            }
        }
    },
    restaurant: {
        type: Schema.Types.ObjectId,
        ref: 'Restaurants',
        index: true
    },
    admin: {
        type: Schema.Types.ObjectId,
        ref: 'Users',
        index: true
    },
    deleted: {
        type: Boolean,
        default: false,
        index: true
    }
}, {timestamps: true, versionKey: false});

module.exports = mongoose.model('Schedules', scheduleSchema);