const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let noteSchema = new Schema({
    customer: {
        type: Schema.Types.ObjectId,
        ref: 'Customers',
        index: true,
    },
    restaurant: {
        type: Schema.Types.ObjectId,
        ref: 'Restaurants',
        index: true,
    },
    notes: {
        customer: {
            type: String,
            default: null
        },
        admin: {
            type: String,
            default: null
        }
    },
    deleted: {
        type: Boolean,
        default: false,
        index: true
    }
}, {timestamps: true, versionKey: false});

module.exports = mongoose.model('Notes', noteSchema);