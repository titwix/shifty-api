const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let reservationSchema = new Schema({
    restaurant: {
        type: Schema.Types.ObjectId,
        ref: 'Restaurants',
        index: true,
        default: null
    },
    date: {
        type: Date,
        default: Date.now
    },
    status: {
        type: String,
        default: 'active',
        index: true,
        match: /^(active|expired)$/
    },
    customers: [
        {
            customer: {
                type: Schema.Types.ObjectId,
                ref: 'Customers',
                index: true,
                default: null
            },
            table: {
                type: Schema.Types.ObjectId,
                ref: 'Tables',
                index: true,
                default: null
            },
            hour: {
                type: String,
                default: null
            },
            status: {
                type: String,
                default: 'active',
                match: /^(active|finished|canceled|pending|paid)$/
            },
            nb_client: {
                type: Number,
                default: 0
            },
            note: {
                type: String,
                default: null
            }
        }
    ],
    deleted: {
        type: Boolean,
        default: false,
        index: true
    }
}, {timestamps: true, versionKey: false});

module.exports = mongoose.model('Reservations', reservationSchema);