const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let userSchema = new Schema({
    email: {
        type: String,
        default: null,
        trim: true,
        required: true
    },
    password: {
        type: String,
        default: null,
        required: true
    },
    identity: {
        firstname: {
            type: String,
            default: null,
            trim: true
        },
        lastname: {
            type: String,
            default: null,
            trim: true
        },
        phone: {
            type: String,
            default: null,
            trim: true
        }
    },
    image: {
        type: String,
        trim: true,
        default: null
    },
    metadata: {
        mail_confirmed: {
            type: Boolean,
            default: false
        },
        accepted_tos: {
            type: Boolean,
            default: false
        }
    },
    deleted: {
        type: Boolean,
        default: false,
        index: true
    }
}, {timestamps: true, versionKey: false});

module.exports = mongoose.model('Users', userSchema);