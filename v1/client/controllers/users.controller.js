/**
 * Modules
 */
 const moment = require('moment');
/**
 * Models
 */
const Users = require('../../models/users.model');

//Routes
module.exports = {
    all: function (req, res) {
 
        Users.find({deleted: false}, (err, users) => {
            if(err){
                return res.status(500).json(global.ERROR("An error occurred"));
            }
            if(users){
                return res.status(200).json(global.SUCCESS(users));
            }
            return res.status(204).json(global.SUCCESS("No user in database"));
        });
 
    },
    single: function (req, res) {
 
        //Recup les données dans des const
        //les paramètres d'url sont dans l'objet "params" et ceux de ta requête dans "body"
        const userId = req.params.userId;

        //Check les données, return 400 si il y a une erreur
        if(!global.VALIDATOR.match(userId)){
            res.status(400).json(global.ERROR("userId not valid", "USER"));
        }
 
        //fait les appels à la bdd avec les bonnes données. Mettre le bon code http de le .status() ( http://www.standard-du-web.com/liste_des_codes_http.php )
        //Le 2ème parametre de global.ERROR c'est le contexte, oublie pas de le mettre sauf si c'est une erreur 500
        //car par défaut il vaut 'INTERNAL', genre pour ce controller j'ai mis "USER"
        //et biensur on fait tout en anglais gros bg là
        Users.findById(userId, (err, user) => {
            if(err){
                return res.status(500).json(global.ERROR("An error occurred"));
            }
            if(user){
                return res.status(200).json(global.SUCCESS(user));
            }
            return res.status(404).json(global.ERROR("No user matching given id", "USER"));
        });
 
    },
    create: function(req, res) {
        //create new user
        const name = req.body.firstname;
        const lastname = req.body.lastname;
        const phone = req.body.phone;
        const email = req.body.email;

        if(!global.VALIDATOR.match(email, 'email')){
            return res.status(400).json(global.ERROR("Email not valid", "Users"));
        }
        if(global.VALIDATOR.empty(lastname) || lastname === ""){
            return res.status(400).json(global.ERROR("Lastname parameter is not valid", "Users"));
        }
        if(global.VALIDATOR.empty(phone) || phone === ""){
            return res.status(400).json(global.ERROR("Phone parameter is not valid", "Users"));
        }
        if(global.VALIDATOR.empty(name) || name === ""){
            return res.status(400).json(global.ERROR("Name parameter is not valid", "Users"));
        }

        Users({
            identity: {
                firstname: name,
                lastname: lastname
            },
            contact: {
                phone: phone,
                mail: email
            }
        }).save((err, user) => {
            //Quand tu dev tu peux mettre "err" dans le retour de la 500, par contre que tu push le code pense bien à les enlever
            //et mettre "An error occurred" car c'est pas ouf de montrer les erreurs serveurs
            if(err) return res.status(500).json(global.ERROR("An error occurred"));
            if(user){
                return res.status(201).json(global.SUCCESS(user));
            }
            return res.status(500).json(global.ERROR("An error occurred"));
        });
    },
    update: function(req, res) {

        const userId = req.params.userId;
        const name = req.body.firstname;
        const lastname = req.body.lastname;
        const phone = req.body.phone;
        const email = req.body.email;

        let update = {
            updatedAt: moment()
        };

        if(!global.VALIDATOR.match(userId)){
            res.status(400).json(global.ERROR("userId not valid", "USER"));
        }
        if(email){
            if(!global.VALIDATOR.match(email, 'email')){
                return res.status(400).json(global.ERROR("Email not valid", "Users"));
            }
            update['contact.mail'] = email;
        }
        if(phone){
            if(global.VALIDATOR.empty(phone) || phone === ""){
                return res.status(400).json(global.ERROR("Phone parameter is not valid", "Users"));
            }
            update['contact.phone'] = phone;
        }
        if(lastname){
            if(global.VALIDATOR.empty(lastname) || lastname === ""){
                return res.status(400).json(global.ERROR("Lastname parameter is not valid", "Users"));
            }
            update['identity.lastname'] = lastname;
        }
        if(name){
            if(global.VALIDATOR.empty(name) || name === ""){
                return res.status(400).json(global.ERROR("Name parameter is not valid", "Users"));
            }
            update['identity.firstname'] = name;
        }

        Users.findOneAndUpdate({_id: userId}, update, (err, update) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));
            if(update){
                return res.status(201).json(global.SUCCESS(update));
            }
            return res.status(500).json(global.ERROR("An error occurred"));
        })
    },
    delete: function (req, res) {

        const userId = req.params.userId;
        
        if(!global.VALIDATOR.match(userId)){
            res.status(400).json(global.ERROR("userId not valid", "USER"));
        }
        
        let update = {
            updatedAt: moment(),
            deleted: true
        };

        Users.findOneAndUpdate({_id: userId}, update, (err, update) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));
            if(update){
                return res.status(201).json(global.SUCCESS(update));
            }
            return res.status(500).json(global.ERROR("An error occurred"));
        })
    }
};
 