/**
 * Models
 */
const Customers = require('../../models/customers.model');
const Notes = require('../../models/notes.model');
 
module.exports = {
    show: function (req, res) {

        const restaurantId = req.params.restaurantId;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)) {
            return res.status(400).json(global.ERROR("Restaurant id is missing or not valid"));
        }

        Notes.find({restaurant: restaurantId}, (err, customers) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));

            if(customers) return res.status(200).json(global.SUCCESS(customers));

            return res.status(é04).end();
        }).populate('customer').sort({'customer.identity.lastname': 'asc'});

    },
    single: function (req, res) {

        const restaurantId = req.params.restaurantId;
        const customerId = req.params.customerId;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)) {
            return res.status(400).json(global.ERROR("Restaurant id is missing or not valid"));
        }
        if(!customerId || !global.VALIDATOR.match(customerId)) {
            return res.status(400).json(global.ERROR("Customer id is missing or not valid"));
        }

        global.ASYNC.waterfall([
            function (done) {
                Customers.findOne({_id: customerId, deleted: false}, (err, customer) => {
                    if(err) return res.status(500).json(global.ERROR("An error occurred"));

                    if(customer) return done(null, customer);

                    return res.status(404).json(global.ERROR("No customer found matching this ID"));
                });
            },
            function (customer, done) {
                Notes.findOne({customer: customerId, restaurant: restaurantId}, (err, note) => {
                    if(err) return res.status(500).json(global.ERROR("An error occurred"));

                    if(!note) return res.status(404).json(global.ERROR("No note found for this customer..."));

                    return done({customer, notes: note.notes});
                })
            }
        ], function(customer) {
            return res.status(200).json(global.SUCCESS(customer));
        });

    },
    create: function (req, res) {

        const restaurantId = req.params.restaurantId;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)) {
            return res.status(400).json(global.ERROR("Restaurant id is missing or not valid"));
        }
 
        const email = req.body.email;
        const firstname = req.body.firstname;
        const lastname = req.body.lastname;
        const phone = req.body.phone;

        if(!email || !global.VALIDATOR.match(email, "email")) {
            return res.status(400).json(global.ERROR("Email id is missing or not valid"));
        }
        if(!firstname) {
            return res.status(400).json(global.ERROR("Firstname is missing"));
        }
        if(!lastname) {
            return res.status(400).json(global.ERROR("Lastname is missing"));
        }
        if(!phone) {
            return res.status(400).json(global.ERROR("Phone is missing"));
        }

        Customers.findOne({email: email, deleted: false}, (err, customer) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));

            if(customer) {
                global.ASYNC.waterfall([
                    function (done) {
                        Notes.findOne({customer: customer._id, restaurant}, (err, note) => {
                            if(err) return res.status(500).json(global.ERROR("An error occurred"));
            
                            return done(null, note);
                        })
                    },
                    function (note, done) {
                        if(note) {
                            return done(customer)
                        } else {
                            Notes({
                                customer: customer._id,
                                restaurant: restaurantId
                            }).save((err, note) => {
                                if(err) return res.status(500).json(global.ERROR("An error occurred"));
    
                                if(note) return done(customer);
    
                                return res.status(500).json(global.ERROR("Cannot assign customer"));
                            });
                        }
                    }
                ], function(customer) {
                    return res.status(201).json(global.SUCCESS(customer));
                });
            } else {
                global.ASYNC.waterfall([
                    function (done) {
                        Customers({
                            email: email,
                            "identity.firstname": firstname,
                            "identity.lastname": lastname,
                            "identity.phone": phone,
                            "metadata.mail_confirmed": true,
                            "metadata.accepted_tos": true
                        }).save((err, customer) => {
                            if(err) return res.status(500).json(global.ERROR("An error occurred"));
                
                            if(customer) {
                                return done(null, customer);
                            }
                
                            return res.status(500).json(global.ERROR("Cannot create customer"));
                        })
                    },
                    function (customer, done) {
                        Notes({
                            customer: customer._id,
                            restaurant: restaurantId
                        }).save((err, note) => {
                            if(err) return res.status(500).json(global.ERROR("An error occurred"));

                            if(note) return done(customer);

                            return res.status(500).json(global.ERROR("Cannot assign customer"));
                        });
                    }
                ], function(customer) {
                    return res.status(201).json(global.SUCCESS(customer));
                });
            }
        });
        
    }
}