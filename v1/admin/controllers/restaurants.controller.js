/**
 * Models
 */
const Restaurants = require('../../models/restaurants.model');
const Schedules = require('../../models/schedules.model');

/**
 * Packages
 */
const fetch = require('node-fetch');

module.exports = {
    single: function (req, res) {

        const userId = req.user._id;
        const restaurantId = req.params.restaurantId;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)){
            return res.status(400).json(global.ERROR("Restaurant Id is missing or not valid", "RESTAURANTS"));
        }

        Restaurants.findOne({admin: userId, _id: restaurantId, deleted: false}, (err, restaurant) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));

            if(restaurant) return res.status(200).json(global.SUCCESS(restaurant));

            return res.status(404).json(globla.ERROR("Restaurant not found"));
        }).populate('schedules')

    },
    show: function (req, res) {
        const userId = req.user._id;

        Restaurants.find({admin: userId, deleted: false}, (err, restaurant) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));

            if(restaurant) return res.status(200).json(global.SUCCESS(restaurant));

            return res.status(404).json(globla.ERROR("Restaurant not found"));
        })
    },
    create: function (req, res) {

        const userId = req.user._id;
        const name = req.body.name;

        const street = req.body.street;
        const city = req.body.city;
        const zipcode = req.body.zipcode;
        const country = req.body.country;

        const phone = req.body.phone;
        const email = req.body.email;

        if(!name){
            return res.status(400).json(global.ERROR("Missing or empty name", "RESTAURANTS"));
        }

        const slug = name.trim().toLowerCase().replaceAll(' ', '-');

        if(!street || !city || !zipcode || !country){
            return res.status(400).json(global.ERROR("Missing or empty address parameter(s)", "RESTAURANTS"));
        }
        if(!phone || !email || !global.VALIDATOR.match(email, 'email')){
            return res.status(400).json(global.ERROR("Missing or not valid contact(s) parameter(s)", "RESTAURANTS"));
        }

        global.ASYNC.waterfall([
            function (done) {
                fetch(`https://api-adresse.data.gouv.fr/search/?q=${street},%20${zipcode}%20${city}`, {method: 'GET'})
                .then(res => res.json())
                .then(response => { return done(null, response); });
            },
            function (response, done) {
                Restaurants({
                    name: name,
                    slug: slug,
                    "contacts.phone": phone,
                    "contacts.email": email,
                    "position.gps.lat": response.features[0].geometry.coordinates[1],
                    "position.gps.long": response.features[0].geometry.coordinates[0],
                    "position.address.street": street,
                    "position.address.zipcode": zipcode,
                    "position.address.city": city,
                    "position.address.country": country,
                    admin: userId
                }).save((err, restaurant) => {
                    if(err) return res.status(500).json(global.ERROR("An error occurred"));

                    if(restaurant) return done(null, restaurant);

                    return res.status(500).json(global.ERROR("Cannot create restaurant"));
                });
            },
            function (restaurant, done) {
                Schedules({
                    restaurant: restaurant._id,
                    admin: userId
                }).save((err, schedule) => {
                    if(err) return res.status(500).json(global.ERROR("An error occurred"));

                    if(schedule) return done(schedule, restaurant);

                    return res.status(500).json(global.ERROR("Cannot create restaurant"));
                });
            }
        ], function (restaurant, schedule) {
            Restaurants.findByIdAndUpdate(restaurant._id, {schedule: schedule._id}, (err) => {
                if(err) return res.status(500).json(global.ERROR("An error occurred"));
    
                return res.status(201).json(global.SUCCESS(restaurant));
            });
        });
    },
    update: function (req, res) {
        
        const userId = req.user._id;
        const restaurantId = req.params.restaurantId;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)) {
            return res.status(500).json(global.ERROR("An error occurred"));
        }

        const body = req.body;
        const data = {};

        //Name, Slug, Description
        if(body.name && !global.VALIDATOR.empty(body.name)) data['name'] = body.name;
        if(body.slug && !global.VALIDATOR.empty(body.slug)) data['slug'] = body.slug;
        if(body.description && !global.VALIDATOR.empty(body.description)) data['description'] = body.description;

        //Pictures
        if(body.pictures) {
            const pictures = body.pictures;
            if(pictures.logo && !global.VALIDATOR.empty(pictures.logo)) data['pictures.logo'] = pictures.logo;
            if(pictures.banner) data['pictures.banner'] = pictures.banner;
        }

        //Contacts
        if(body.contacts) {
            const contacts = body.contacts;
            if(contacts.phone && !global.VALIDATOR.empty(contacts.phone)) data['contacts.phone'] = contacts.logo;
            if(contacts.email && !global.VALIDATOR.match(contacts.email, 'email')) data['contacts.email'] = contacts.email;
        }

        //Address
        if(body.address) {
            const address = body.address;
            if(address.street && !global.VALIDATOR.empty(address.street)) data["position.address.street"] = address.street;
            if(address.zipcode && !global.VALIDATOR.empty(address.zipcode)) data['position.address.zipcode'] = address.zipcode;
            if(address.city && !global.VALIDATOR.empty(address.city)) data['position.address.city'] = address.city;
            if(address.country && !global.VALIDATOR.empty(address.country)) data['position.address.country'] = address.country;
        }

        global.ASYNC.waterfall([
            function (done) {
                Restaurants.findOne({admin: userId, _id: restaurantId, deleted: false}, (err, restaurant) => {
                    if(err) return res.status(500).json(global.ERROR("An error occurred"));
        
                    if(restaurant) return done(null, restaurant);
        
                    return res.status(404).json(globla.ERROR("Restaurant not found"));
                })
            },
            function (restaurant, done) {
                if(data["position.address.street"] || data["position.address.zipcode"] || data["position.address.city"]) {

                    let street = data["position.address.street"] ? data["position.address.street"] : restaurant.position.address.street;
                    let zipcode = data["position.address.zipcode"] ? data["position.address.zipcode"] : restaurant.position.address.zipcode;
                    let city = data["position.address.city"] ? data["position.address.city"] : restaurant.position.address.city;

                    let address = `${street},%20${zipcode}%20${city}`;

                    fetch(`https://api-adresse.data.gouv.fr/search/?q=${address}`, {method: 'GET'})
                    .then(res => res.json())
                    .then(response => {
                        data["position.gps.lat"] = response.features[0].geometry.coordinates[1];
                        data["position.gps.long"] = response.features[0].geometry.coordinates[0];

                        return done(null, restaurant); 
                    });
                } else {
                    return done(null, restaurant);
                }
            },
            function (restaurant, done) {
                if(data['slug']) {
                    Restaurants.findOne({slug: data['slug']}, (err, restaurant) => {
                        if(err) return res.status(500).json(global.ERROR("An error occurred"));
            
                        if(restaurant) res.status(400).json(global.ERROR("Slug already used"));
            
                        return done(restaurant);
                    })
                } else {
                    return done(restaurant);
                }
            }
        ], function(restaurant) {
            Restaurants.findOneAndUpdate({admin: userId, _id: restaurantId}, data, {new: true}, (err, newRestaurant) => {
                if(err) return res.status(500).json(global.ERROR("An error occurred"));

                if(newRestaurant) return res.status(200).json(global.SUCCESS(newRestaurant));

                return res.status(500).json(global.ERROR("Cannot update restaurant"));
            });
        });
    },
    delete: function (req, res) {

        const userId = req.user._id;
        const restaurantId = req.params.restaurantId;

        Restaurants.findByIdAndUpdate({admin: userId, _id: restaurantId}, {deleted: true}, (err, deleteRestaurant) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));

            if(deleteRestaurant) return res.status(200).json(global.SUCCESS("Restaurant deleted with success"));

            return res.status(500).json(global.ERROR("Cannot delete restaurant"));
        });
    }
}