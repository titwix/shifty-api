/**
 * Models
 */
const Users = require('../../models/users.model');
const Sessions = require('../../models/sessions.model');

/**
 * Helpers
 */
const SessionsHelper = require('../../helpers/sessions.helper');

/**
 * Packages
 */
const bcrypt = require('bcrypt');

//Routes
module.exports = {
    create: function (req, res) {

        const email = req.body.email;
        const password = req.body.password;
        const permanent = req.body.permanent;

        if (!email || !password) {
            return res.status(400).json(global.ERROR('Missing email and/or password parameter', 'LOGIN'));
        }
        if (!global.VALIDATOR.match(email, "email")) {
            return res.status(400).json(global.ERROR('Email format incorrect', 'LOGIN'));
        }
        if (!global.VALIDATOR.match(password, "password")) {
            return res.status(400).json(global.ERROR('Password format incorrect', 'LOGIN'));
        }
        if(permanent && typeof permanent !== "boolean") permanent = false;

        global.ASYNC.waterfall([
            function (done) {
                Users.findOne({email: email, deleted: false}, (err, user) => {
                    if(err) return res.status(500).json(global.ERROR("An error occurred"));

                    if(user) return done(null, user);

                    return res.status(401).json(global.ERROR("No user found with this email", "LOGIN"));
                })
            },
            function (user, done) {
                bcrypt.compare(password, user.password, (err, resBcrypt) => {
                    if(err) return res.status(500).json(global.ERROR("An error occurred"));

                    if(resBcrypt) return done(user);

                    return res.status(401).json(global.ERROR("Password not matching", "LOGIN"));
                });
            }
        ], function (user) {
            SessionsHelper.set(req, user, 7, (err, session) => {
                if(err) return res.status(500).json(global.ERROR("An error occurred"));

                if(session) return res.status(200).json(global.SUCCESS({token: session.JWT}));

                return res.status(500).json(global.ERROR("Cannot create the session"));
            });
        });
    },
    delete: function (req, res) {

        Sessions.deleteOne({_id: req.user.session._id}, function (err, deleteInstance) {
            if (err) return res.status(500).json(global.ERROR("An error occurred"));

            if (deleteInstance) {
                return res.status(200).json(global.SUCCESS("Session successfully deleted"));
            } else {
                return res.status(500).json(global.ERROR("Cannot destroy the session."));
            }
        });
    }
} 