/**
 * Models
 */
const Users = require('../../models/users.model');

/**
 * Packages
 */
const bcrypt = require('bcrypt');

//Routes
module.exports = {
    create: function (req, res) {

        console.log(req.body)

        const email = req.body.email;
        const password = req.body.password;
        const firstname = req.body.firstname;
        const lastname = req.body.lastname;
        const phone = req.body.phone;
        const accepted_tos = req.body.accepted_tos;

        if(!accepted_tos){
            return res.status(400).json(global.ERROR("Please accepte tos to create an account", "USERS"));
        }
        if(!email || !global.VALIDATOR.match(email, "email")){
            return res.status(400).json(global.ERROR("Email is missing or not valid", "USERS"));
        }
        if(!password || !global.VALIDATOR.match(password, "password")){
            return res.status(400).json(global.ERROR("Password is missing or not valid", "USERS"));
        }
        if(!firstname || !lastname || !phone){
            return res.status(400).json(global.ERROR("Missing parameters (firstname, lastname or phone)", "USERS"));
        }

        bcrypt.hash(password, 10, (err, hash) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));

            Users({
                email: email,
                password: hash,
                "identity.firstname": firstname,
                "identity.lastname": lastname,
                "identity.phone": phone,
                "metadata.accepted_tos": accepted_tos
            }).save((err, user) => {
                if(err) return res.status(500).json(global.ERROR("An error occurred"));

                if(user) {
                    return res.status(201).json(global.SUCCESS(user));
                }

                return res.status(500).json(global.ERROR("Cannot create user"));
            })
        })
    },
    me: (req, res) => {

        Users.findById(req.user._id, '-password', function (err, userFound) {
            if (err) return res.status(500).json(global.ERROR('An error occurred'));

            if (userFound) return res.status(200).json(global.SUCCESS(userFound));

            return res.status(404).json(global.ERROR('User not found', 'USERS'));
        })
    }
};
