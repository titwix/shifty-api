/** 
 * Others
 */
const moment = require('moment');

/**
 * Models
 */
const Reservations = require('../../models/reservations.model');

module.exports = {
    single: (req, res) => {
        const restaurantId = req.params.restaurantId;
        const reservationId = req.params.reservationId;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)) {
            return res.status(400).json(global.ERROR("Restaurant id is missing or not valid"));
        }

        if(!reservationId || !global.VALIDATOR.match(reservationId)) {
            return res.status(400).json(global.ERROR("Reservation id is missing or not valid"));
        }

        Reservations.find({restaurant: restaurantId, _id: reservationId}, (err, reservation) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));

            if(reservation) return res.status(200).json(global.SUCCESS(reservation));

            return res.status(404).json(global.ERROR("No reservation found matching this ID"));
        }).populate('customers.customer').populate('customers.table')
    },
    show: (req, res) => {

        const restaurantId = req.params.restaurantId;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)) {
            return res.status(400).json(global.ERROR("Restaurant id is missing or not valid"));
        }

        Reservations.find({restaurant: restaurantId}, (err, reservations) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));

            if(reservations) return res.status(200).json(global.SUCCESS(reservations));

            return res.status(204).end();
        }).populate('customers.customer').populate('customers.table')
    },
    create: (req, res) => {

        const restaurantId = req.params.restaurantId;
        const tableId = req.body.tableId;
        const customerId = req.body.customerId;
        const unformattedDate = req.body.date;
        const hour = req.body.hour;
        const nb_client = req.body.nb_client;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)) {
            return res.status(400).json(global.ERROR("Restaurant id is missing or not valid", "RESERVATIONS"));
        }
        if(!tableId || !global.VALIDATOR.match(tableId)) {
            return res.status(400).json(global.ERROR("Table id is missing or not valid", "RESERVATIONS"));
        }
        if(!customerId || !global.VALIDATOR.match(customerId)) {
            return res.status(400).json(global.ERROR("Customer id is missing or not valid", "RESERVATIONS"));
        }

        let date = null;

        try {
            date = moment(unformattedDate)
        } catch (e) {
            return res.status(400).json(global.ERROR("Invalid date format", "RESERVATIONS"));
        }

        Reservations.findOne({restaurant: restaurantId, date, deleted: false}, (err, reservation) => {
            if (err) return res.status(500).json(global.ERROR("An error occurred"));

            if(!reservation) {
                Reservations({
                    restaurant: restaurantId,
                    date,
                    customers: [
                        {
                            customer: customerId,
                            table: tableId,
                            hour,
                            nb_client
                        }
                    ]
                }).save((err, createdReservation) => {
                    if (err) return res.status(500).json(global.ERROR("An error occurred"));

                    if (createdReservation) return res.status(201).json(global.SUCCESS(createdReservation));

                    return res.status(500).json(global.ERROR("Cannot create reservation"))
                });
            } else {
                global.ASYNC.waterfall([
                    function (done) {
                        const newCustomersArray = reservation.customers;

                        newCustomersArray.push({
                            customer: customerId,
                            table: tableId,
                            hour,
                            nb_client
                        })

                        return done(newCustomersArray);
                    }
                ], function (customersArray) {

                    Reservations.findOneAndUpdate({restaurant: restaurantId, date, deleted: false}, {customers: customersArray}, {new: true}, (err, updatedReservation) => {
                        if (err) return res.status(500).json(global.ERROR("An error occurred"));

                        if (updatedReservation) return res.status(200).json(global.SUCCESS(updatedReservation));

                        return res.status(500).json(global.ERROR("Cannot create reservation"));
                    })
                });
            }
            
        });

    }
}