/**
 * Models
 */
const Waiters = require('../../models/waiters.model');

/**
 * Packages
 */
const generator = require('generate-password')
const bcrypt = require('bcrypt');

module.exports = {
    show: function (req, res) {

        const userId = req.user._id;
        const restaurantId = req.params.restaurantId;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)) {
            return res.status(400).json(global.ERROR("Restaurant id is missing or not valid"));
        }

        Waiters.find({restaurant: restaurantId, admin: userId}, (err, waiters) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));

            if(waiters) return res.status(200).json(global.SUCCESS(waiters));

            return res.status(204).json(global.SUCCESS("No waiter(s) found"));
        });
    },
    single: function (req, res) {
        
        const userId = req.user._id;
        const restaurantId = req.params.restaurantId;
        const waiterId = req.params.waiterId;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)) {
            return res.status(400).json(global.ERROR("Restaurant id is missing or not valid"));
        }
        if(!waiterId || !global.VALIDATOR.match(waiterId)) {
            return res.status(400).json(global.ERROR("Waiter id is missing or not valid"));
        }

        Waiters.findOne({_id: waiterId, restaurant: restaurantId, admin: userId}, (err, waiter) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));

            if(waiter) return res.status(200).json(global.SUCCESS(waiter));

            return res.status(204).json(global.SUCCESS("No waiter found matching this ID"));
        });
    },
    create: function (req, res) {

        const userId = req.user._id;
        const restaurantId = req.params.restaurantId;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)) {
            return res.status(400).json(global.ERROR("Restaurant id is missing or not valid"));
        }

        const email = req.body.email;
        const firstname = req.body.firstname;
        const lastname = req.body.lastname;

        if(!email || !global.VALIDATOR.match(email, "email")) {
            return res.status(400).json(global.ERROR("Email is missing or not valid"));
        }
        if(!firstname || !lastname) {
            return res.status(400).json(global.ERROR("Firstname or lastname missing(s)"));
        }

        const password = generator.generate({length: 10, strict: true, symbols: true, numbers: true});

        bcrypt.hash(password, 10, (err, hash) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));

            Waiters({
                email: email,
                password: hash,
                "identity.firstname": firstname,
                "identity.lastname": lastname,
                restaurant: restaurantId,
                admin: userId
            }).save((err, waiter) => {
                if(err) return res.status(500).json(global.ERROR("An error occurred"));
    
                if(waiter) return res.status(201).json(global.SUCCESS(waiter));
    
                return res.status(500).json(global.ERROR("Cannot create waiter"));
            });
        })
        
    },
    update: function (req, res) {

        const userId = req.user._id;
        const restaurantId = req.params.restaurantId;
        const waiterId = req.params.waiterId;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)) {
            return res.status(400).json(global.ERROR("Restaurant id is missing or not valid"));
        }
        if(!waiterId || !global.VALIDATOR.match(waiterId)) {
            return res.status(400).json(global.ERROR("Waiter id is missing or not valid"));
        }

        const data = {};

        const email = req.body.email;
        const firstname = req.body.firstname;
        const lastname = req.body.lastname;
        const image = req.body.image;

        if(email || global.VALIDATOR.match(email)) data['email'] = email;
        if(firstname) data['identity.firstname'] = firstname;
        if(lastname) data['identity.lastname'] = lastname;
        if(image) data['image'] = image;
        
        Waiters.findOneAndUpdate({_id: waiterId, restaurant: restaurantId, admin: userId}, data, {new: true}, (err, waiter) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));

            if(waiter) return res.status(200).json(global.SUCCESS(waiter));

            return res.status(500).json(global.ERROR("Cannot update waiter"));
        });
    },
    delete: function(req, res) {

        const userId = req.user._id;
        const restaurantId = req.params.restaurantId;
        const waiterId = req.params.waiterId;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)) {
            return res.status(400).json(global.ERROR("Restaurant id is missing or not valid"));
        }
        if(!waiterId || !global.VALIDATOR.match(waiterId)) {
            return res.status(400).json(global.ERROR("Waiter id is missing or not valid"));
        }

        Waiters.findOneAndUpdate({_id: waiterId, restaurant: restaurantId, admin: userId}, {deleted: true}, (err, waiter) => {
            if(err) return res.status(500).json(global.ERROR("An error occurred"));

            if(waiter) return res.status(200).json(global.SUCCESS("Waiter deleted with success"));

            return res.status(500).json(global.ERROR("Cannot delete waiter"));
        });
    }
}