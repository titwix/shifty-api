/**
 * Models
 */
 const Tables = require('../../models/tables.model');

module.exports = {
    show: (req, res) => {
        const restaurantId = req.params.restaurantId;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)) {
            return res.status(400).json(global.ERROR("Restaurant id is missing or not valid"));
        }

        Tables.find({restaurant: restaurantId, deleted: false}, (err, tables) => {
            if (err) return res.status(500).json(global.ERROR("An error occurred"));

            if (tables) return res.status(200).json(global.SUCCESS(tables));

            return res.status(204).end();
        });
    },
    create: (req, res) => {
        const restaurant = req.params.restaurantId;
        const name = req.body.name;
        const number = req.body.number;
        const seats = req.body.seats;

        const restaurantId = req.params.restaurantId;

        if(!restaurantId || !global.VALIDATOR.match(restaurantId)) {
            return res.status(400).json(global.ERROR("Restaurant id is missing or not valid"));
        }

        Tables({
            name,
            number,
            restaurant,
            seats
        }).save((err, table) => {
            if (err) return res.status(500).json(global.ERROR("An error occurred"));

            if (table) return res.status(201).json(global.SUCCESS(table));

            return res.status(500).json(global.ERROR("Cannot create table"));
        })
    }
}