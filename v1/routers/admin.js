/**
 * Module dependencies.
 */
const express = require('express');

/**
 * Controllers
 */
const Users = require('../admin/controllers/users.controller');
const Sessions = require('../admin/controllers/sessions.controller');
const Restaurants = require('../admin/controllers/restaurants.controller');
const Waiters = require('../admin/controllers/waiters.controller');
const Customers = require('../admin/controllers/customers.controller');
const Reservations = require('../admin/controllers/reservations.controller');
const Tables = require('../admin/controllers/tables.controller');

/**
 * Middlewares
 */
const SessionControl = require('../middlewares/controls.mw').session;
 
//Router
exports.router = (function () {
  const Router = express.Router();

  /**
   * Sessions
  */
  Router.route('/sessions').post(Sessions.create)
  Router.route('/sessions').delete(SessionControl, Sessions.delete)

  /**
   * Restaurants
   */
  Router.route('/restaurants').post(SessionControl, Restaurants.create)
  Router.route('/restaurants').get(SessionControl, Restaurants.show)
  Router.route('/restaurants/:restaurantId').get(SessionControl, Restaurants.single)
  Router.route('/restaurants/:restaurantId').put(SessionControl, Restaurants.update)
  Router.route('/restaurants/:restaurantId').delete(SessionControl, Restaurants.delete)

  /**
   * Waiters
   */
  Router.route('/waiters/:restaurantId').get(SessionControl, Waiters.show)
  Router.route('/waiters/:restaurantId').post(SessionControl, Waiters.create)
  Router.route('/waiters/:restaurantId/:waiterId').get(SessionControl, Waiters.single)
  Router.route('/waiters/:restaurantId/:waiterId').put(SessionControl, Waiters.update)
  Router.route('/waiters/:restaurantId/:waiterId').delete(SessionControl, Waiters.delete)

  /**
   * Customers
   */
  Router.route('/customers/:restaurantId/:customerId').get(SessionControl, Customers.single);
  Router.route('/customers/:restaurantId').post(SessionControl, Customers.create);
  Router.route('/customers/:restaurantId').get(SessionControl, Customers.show);

  /**
   * Reservations
   */
  Router.route('/reservations/:restaurantId').get(SessionControl, Reservations.show);
  Router.route('/reservations/:restaurantId').post(SessionControl, Reservations.create);
  Router.route('/reservations/:restaurantId/:reservationId').get(SessionControl, Reservations.single);

  /**
   * Tables
   */
  Router.route('/tables/:restaurantId').get(SessionControl, Tables.show);
  Router.route('/tables/:restaurantId').post(SessionControl, Tables.create);

  /**
   * Users
   */
  Router.route('/users').post(Users.create)
  Router.route('/users/me').get(SessionControl, Users.me)
 
  return Router;
})(); 