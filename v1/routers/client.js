/**
 * Module dependencies.
 */
const express = require('express');
/**
 * Controllers
 */
const Users = require('../client/controllers/users.controller');
  
//Router
exports.router = (function () {
  const Router = express.Router();
  
  Router.route('/users').get(Users.all);
  Router.route('/users').post(Users.create);
  Router.route('/users/:userId').get(Users.single);
  Router.route('/users/:userId').put(Users.update);
  Router.route('/users/:userId').delete(Users.delete);
  
  return Router;
})(); 