/**
 * Models
 */
const Sessions = require('../models/sessions.model');
const SessionsCustomer = require('../models/sessions.customer.model');

/**
 * Packages
 */
const device = require('device');
const jwt = require('jsonwebtoken');

/**
 * Key
 */
const signKey = process.env.JWT_SIGN;

module.exports = {
    set: async function (req, user, expirationDays = 0.2, callback) {
        const ip = req.header('x-forwarded-for') || req.connection.remoteAddress
        const agent = req.get('user-agent')
        const getDevice = device(agent)
        let jwtGenerated = null;

        const data = {
            ip: ip,
            device: getDevice.type,
            agent: agent
        }

        const ExpirationDate = new Date(new Date().getTime() + (86400000 * expirationDays))
        const ExpirationDateSecond = 86400000 * expirationDays;
        
        try {
            jwtGenerated = jwt.sign({userId: user._id}, signKey, {expiresIn: ExpirationDateSecond});
        } catch(err) {
            return callback(err);
        }

        Sessions({
            user: user._id,
            JWT: jwtGenerated,
            metadata: {
                device: data.device,
                ip: data.ip
            },
            expireAt: ExpirationDate
        }).save((err, session) => {
            if (err) return callback(err);
            
            return callback(null, session);
        })
    }
}